﻿namespace Projeto01.Model
{
    public class Produto
    {
        public int IdProduto { get; set; }

        public string Nome { get; set; }

        public double Preco { get; set; }

        public Produto()
        {
            //Classe padrão
        }

    }
}
