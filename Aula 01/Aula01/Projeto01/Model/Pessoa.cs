﻿namespace Projeto01.Model
{
    public class Pessoa
    {
        public int IdPessoa { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        /* Exemplo do uso de Construtores */
        public Pessoa()
        {
            //Construtor Padrão
        }

        /* Exemplo em como usar os parâmetros de um determinado construtor criado */
        public Pessoa(int IdPessoa, string Nome, string Email)
        {
            this.IdPessoa = IdPessoa;
            this.Nome = Nome;
            this.Email = Email;
        }
    }
}
