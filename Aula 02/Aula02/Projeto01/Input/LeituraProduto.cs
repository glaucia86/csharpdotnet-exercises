﻿using System;
using System.Text.RegularExpressions; //Expressão Regular

namespace Projeto01.Input
{
    public class LeituraProduto
    {
        public int LerCodigoProduto()
        {
            try {
                    Console.Write("Informe o Código do Produto............: ");
                    int codigo = Convert.ToInt32(Console.ReadLine());

                    /* Se o código digitado for maior do que 0 então.... retorne o código digitado... senão.... */
                    if (codigo > 0) {
                        return codigo;
                    }

                    else {
                        throw new Exception("Código Inválido! Digite novamente");
                    }
            }
            catch (Exception e) {
                Console.WriteLine("Error.........: " + e.Message);
                return LerCodigoProduto();
            }
        }

        public string LerNomeProduto()
        {
            try {
                    Console.Write("Informe o Nome do Produto..............: ");
                    string nome = Console.ReadLine();

                    var regex = new Regex("^[A-Za-zÀ-Üà-ü0-9\\s]{3,30}$");

                if (regex.IsMatch(nome))
                {
                    return nome;
                }
            }
            catch (Exception e) {
                Console.WriteLine("Error...........: " + e.Message);
                
            }
            return LerNomeProduto();
        }

        public double LerPrecoProduto()
        {
            try {
                Console.Write("Informe o Preço do Produto..............: ");
                double preco = Convert.ToDouble(Console.ReadLine());

                if (preco > 0 && preco <= 10000) {
                    return preco;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error.................: " + e.Message);
                return LerPrecoProduto();
            }

            return LerPrecoProduto();
        }
    }
}
