﻿using System;
using Projeto01.Model; // importando os dados
using Projeto01.Input; //importando os dados
using Projeto01.Persistence; //importando os dados

namespace Projeto01
{
    class Program
    {
        static void Main(string[] args)
        {
            var p   = new Produto();
            var lp  = new LeituraProduto();

            p.IdProduto = lp.LerCodigoProduto();
            p.Nome      = lp.LerNomeProduto();
            p.Preco     = lp.LerPrecoProduto();

            Console.WriteLine("\nDados do Produto");
            Console.WriteLine("Código.........................: " + p.IdProduto);
            Console.WriteLine("Nome...........................: " + p.Nome);
            Console.WriteLine("Preço..........................: " + p.Preco);

            var arquivo = new Arquivo();
            arquivo.GravarProduto(p);

            Console.ReadKey();
        }
    }
}
