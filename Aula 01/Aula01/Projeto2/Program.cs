﻿using System;
using Projeto2.Model;

namespace Projeto2
{
    class Program
    {
        static void Main(string[] args)
        {
            var pf = new PessoaFisica
            {
                IdCliente = 1,
                Nome = "Glaucia Lemos",
                Cpf = "111.111.111-11",

                Endereco = new Endereco()
                {
                    Logradouro = "Scalabrini Ortiz, 3656",
                    Estado = "Rio de Janeiro"
                }
            };

            Console.WriteLine("Pessoa Física...................: " + pf.Nome);
            Console.WriteLine("CPF.............................: " + pf.Cpf);
            Console.WriteLine("Pessoa Física...................: " + pf.Endereco.Logradouro);
            Console.WriteLine("Pessoa Física...................: " + pf.Endereco.Estado);

            Console.ReadKey();
        }
    }
}
