# C# .NET Exercises

Exercícios para fins de estudo voltado para a C# .NET. Abaixo segue o sumário e os códigos de cada exercício realizado.

### 1) [Aula 1](https://github.com/glaucia86/csharpdotnet-exercises/tree/master/Aula%2001/Aula01)
- Assuntos Abordados:
  * Introdução a .NET
  * Introdução a Orientação a Objetos 
  * Design Patterns - Singleton

### 2) [Aula 2](https://github.com/glaucia86/csharpdotnet-exercises/tree/master/Aula%2002/Aula02)
- Assuntos Abordados:
  * Modelagem de Classes
  * Entrada de dados por Console
  * Tratamento de Exceções
  * Manipulação de Arquivos

