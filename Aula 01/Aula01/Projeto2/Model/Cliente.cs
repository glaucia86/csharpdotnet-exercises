﻿namespace Projeto2.Model
{
    public class Cliente
    {
        public int IdCliente { get; set; }

        public string Nome { get; set; }

        public Endereco Endereco; /* Aqui estamos realizando a associação entre classes.
                                    * Ou seja: Um cliente TEM Endereço. */

        public Cliente()
        {
            //Construtor Default
        }
    }

}
