﻿namespace Projeto01.Model
{
    /* Exemplo de Classe usando encapsulamento {get;set;} */
    public class Aluno
    {
        private double nota1;
        private double nota2;

        public double Nota1
        {
            set { nota1 = value;  }
        }

        public double Nota2
        {
            set { nota2 = value; }
        }

        public double Media
        {
            get { return (nota1 + nota2/2); }
        }

    }
}
