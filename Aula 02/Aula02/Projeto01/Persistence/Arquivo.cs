﻿using System;
using Projeto01.Model; //importando
using System.IO;

namespace Projeto01.Persistence
{
    public class Arquivo
    {
        /* Método responsável por gravar dados do Produto no arquivo excel */
        public void GravarProduto(Produto p)
        {
            try
            {
                /* Classe responsável por gravar os dados no arquivo */
                var sw = new StreamWriter("C:\\temp\\estoque.csv", true);

                /* Escrita no Arquivo */
                sw.WriteLine(p.IdProduto + ";" + p.Nome + ";" + p.Preco);
                sw.Close(); /* Fechar o arquivo */

                //Se tudo der certo.....
                Console.WriteLine("\nDados gravados com Sucesso!!");
            }
            catch (Exception e)
            {
                throw new Exception("Error........: " + e.Message);
            }
        }
    }
}
