﻿namespace Projeto2.Model
{
    public class Endereco
    {
        public string Logradouro { get; set; }

        public string Estado { get; set; }
    }
}
