﻿using System;
using System.Security.Cryptography.X509Certificates;
using Projeto01.Model; //aqui estou importando a classe contida na pasta Model

namespace Projeto01
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Entrada de Dados da classe modelo - Pessoa 
            var p = new Pessoa
            {
                IdPessoa = 1,
                Nome = "Glaucia Lemos",
                Email = "skinclear86@gmail.com"
            };
            */

            var p = new Pessoa(1, "Jake Lemos", "jake@gmail.com");

            //Saída - Impressão:
            Console.WriteLine("Id da Pessoa é...................: " + p.IdPessoa);
            Console.WriteLine("Nome.............................: " + p.Nome);
            Console.WriteLine("Email............................: " + p.Email);

            Console.WriteLine("C# .NET Exercises!");

            Console.ReadKey();

        }
    }
}
